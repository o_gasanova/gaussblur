//
// Created by Ольга Гасанова on 02.07.2021.
//

#include "GaussBlur.h"
#include <jni.h>
#include <string>
#include <jni.h>
#include <math.h>
#include <vector>
#include <chrono>
#include <iostream>

using namespace std::chrono;

const int numChannels = 4;
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))


std::vector<int> CreateGausianBoxes(double sigma, int n) {
    double idealFilterWidth = sqrt((12 * sigma * sigma / n) + 1);

    int filterWidth = (int) floor(idealFilterWidth);
    if (filterWidth % 2 == 0) {
        filterWidth--;
    }

    int filterWidthU = filterWidth + 2;

    double mIdeal = (12 * sigma * sigma - n * filterWidth * filterWidth - 4 * n * filterWidth - 3 * n) / (-4 * filterWidth - 4);
    double m = round(mIdeal);

    std::vector<int> result;

    for (int i = 0; i < n; i++) {
        result.push_back(i < m ? filterWidth : filterWidthU);
    }

    return result;
}

int get_integral(int* scl, int x0, int y0, int x1, int y1, int w, int h)
{
    int sum = scl[y1 * h * numChannels + x1];

    if (x0 >= numChannels)
        sum -= scl[y1* h * numChannels + x0 - numChannels];
    if (y0 > 0)
        sum -= scl[(y0 - 1) * h * numChannels + x1];
    if (x0 >= numChannels && y0 > 0)
        sum += scl[(y0 - 1) * h * numChannels + x0 - numChannels];

    return sum;
}

void boxH (int* scl, int* tcl, int w, int h, int r) {
    for(int i=0; i<h; i++)
        for(int j=0; j<w; j++)  {
            int val = 0;
            for(int ix=j-r*numChannels; ix<j+(r+1)*numChannels; ix=ix+numChannels) {
                int x = min(w-1, max(0, ix));
                val += scl[i*w+x];
            }
            tcl[i*w+j] = val/(r+r+1);
        }
}

void boxV (int* scl, int* tcl, int w, int h, int r) {
    for(int i=0; i<h; i++)
        for(int j=0; j<w; j++) {
            int val = 0;
            for(int iy=i-r; iy<i+r+1; iy++) {
                int y = min(h-1, max(0, iy));
                val += scl[y*w+j];
            }
            tcl[i*w+j] = val/(r+r+1);
        }
}

void boxF(int* source, int* output, int width, int height, int radius) {
    boxH(source, output, (width) * numChannels, height, radius);
    boxV(output, source, (width) * numChannels, height, radius);
}

void satBlur (int* scl, int* tcl, int w, int h, int* r) {
    float weight[3] = {0.5,0.3,0.2};
    float iarr[3];
    for (int i = 0; i < 3; i ++)
    {
        float ks = r[i] + r[i] + 1;
        iarr[i] = 1.f / (ks * ks);
    }

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w * numChannels; x++) {

            for(int ir = 0; ir < 3; ir++)
            {
                int i00 = max(x - r[ir] * numChannels, 0);
                int i11 = max(y - r[ir], 0);
                int i22 = min(x + r[ir] * numChannels, w * numChannels - 1);
                int i33 = min(y + r[ir], h - 1);

                int sumR = (int) get_integral(scl,
                                              i00, i11, i22, i33,
                                              w, h);



                int res = round(sumR * iarr[ir]);
                tcl[y * h * numChannels + x] += res*weight[ir];
            }

            /* if(x >= numChannels && y> 0)
                 res -= tcl[x - numChannels + (y - 1) * h * numChannels];
             if(y > 0)
                 res += tcl[x + (y - 1) * h * numChannels];
             if(x >= numChannels)
                 res += tcl[x - numChannels + y * h * numChannels];*/


        }
    }
}

int gl_I = 0;

void boxSlidingH (int* scl, int* tcl, int w, int h, int r) {
    float iarr = 1.0 / (r+r+1);
    for(gl_I=0; gl_I<h; gl_I++) {
        int ti = gl_I * w, li = ti, ri = ti + r * 4;
        int fv = scl[ti], lv = scl[ti + w - 1], val = (r + 1) * fv;

        int acc0 = (r + 1) * scl[gl_I * w];
        int acc1 = (r + 1) * scl[gl_I * w + 1];
        int acc2 = (r + 1) * scl[gl_I * w + 2];
        int acc3 = (r + 1) * scl[gl_I * w + 3];

        for (int j = 0; j < r * numChannels; j += 4) {
            acc0 += scl[ti + j];
            acc1 += scl[ti + j + 1];
            acc2 += scl[ti + j + 2];
            acc3 += scl[ti + j + 3];
        }
        for (int j = 0; j <= r * 4; j += 4) {
            acc0 += scl[ri++] - scl[gl_I * w];
            acc1 += scl[ri++] - scl[gl_I * w + 1];
            acc2 += scl[ri++] - scl[gl_I * w + 2];
            acc3 += scl[ri++] - scl[gl_I * w + 3];

            tcl[ti++] = round(acc0 * iarr);
            tcl[ti++] = round(acc1 * iarr);
            tcl[ti++] = round(acc2 * iarr);
            tcl[ti++] = round(acc3 * iarr);
        }
        for (int j = r * 4 + 4; j < w - 4; j += 4) {
            acc0 += scl[ri++] - scl[li++];
            acc1 += scl[ri++] - scl[li++];
            acc2 += scl[ri++] - scl[li++];
            acc3 += scl[ri++] - scl[li++];

            tcl[ti++] = round(acc0 * iarr);
            tcl[ti++] = round(acc1 * iarr);
            tcl[ti++] = round(acc2 * iarr);
            tcl[ti++] = round(acc3 * iarr);
        }
        for (int j = w - r * 4; j < w; j+=4) {
            acc0 += scl[ti + w] - scl[li++];
            acc1 += scl[ti + w - 1] - scl[li++];
            acc2 += scl[ti + w - 2] - scl[li++];
            acc3 += scl[ti + w - 3] - scl[li++];

            tcl[ti++] = round(acc0 * iarr);
            tcl[ti++] = round(acc1 * iarr);
            tcl[ti++] = round(acc2 * iarr);
            tcl[ti++] = round(acc3 * iarr);
        }
    }
}

void boxSlidingV (int* scl, int* tcl, int w, int h, int r) {
    float iarr = 1.f / (r+r+1.f);
    for(int i=0; i<w; i++) {
        int ti = i, li = ti, ri = ti+r*w;
        int fv = scl[ti], lv = scl[ti+w*(h-1)], val = (r+1)*fv;
        for(int j=0; j<r; j++) val += scl[ti+j*w];
        for(int j=0  ; j<=r ; j++) { val += scl[ri] - fv     ;  tcl[ti] = round(val*iarr);  ri+=w; ti+=w; }
        for(int j=r+1; j<h-r; j++) { val += scl[ri] - scl[li];  tcl[ti] = round(val*iarr);  li+=w; ri+=w; ti+=w; }
        for(int j=h-r; j<h  ; j++) { val += lv      - scl[li];  tcl[ti] = round(val*iarr);  li+=w; ti+=w; }
    }
}

void boxSlidingF(int* source, int* output, int width, int height, int radius) {
    boxSlidingH(source, output, (width) * numChannels, height, radius);
    boxSlidingV(output, source, (width) * numChannels, height, radius);
}

void boxSlidingCpu(int* source, int* output, int width, int height, int radius) {
    std::vector<int> gaussianBoxes = CreateGausianBoxes(radius, 3);
    boxSlidingF(source, output, width, height, (gaussianBoxes[0] - 1) / 2);
    boxSlidingF(source, output, width, height, (gaussianBoxes[1] - 1) / 2);
    boxSlidingF(source, output, width, height, (gaussianBoxes[2] - 1) / 2);
}

void satBoxCpu(int* source, int* output, int width, int height, int radius) {
    int r[3] = {3, 7, 15};
    satBlur(source, output, width, height, r);
}

void boxCpu(int* source, int* output, int width, int height, int radius) {
    std::vector<int> gaussianBoxes = CreateGausianBoxes(radius, 3);
    boxF(source, output, width, height, (gaussianBoxes[0] - 1) / 2);
    boxF(source, output, width, height, (gaussianBoxes[1] - 1) / 2);
    boxF(source, output, width, height, (gaussianBoxes[2] - 1) / 2);
}

milliseconds exec_time;
int dwDuration;

enum Approach
{
    BoxCpu,
    BoxSlidingCPU,
    SatBoxCPU,
    OpenCVCPU,
    ExtendedBoxGPU,
};

extern "C" JNIEXPORT jintArray JNICALL
Java_com_example_memorybandwidth_MyGLRenderer_BoxCpuCommonCpp(JNIEnv *env,
                                                              jobject thiz,
                                                              jintArray oldArray,
                                                              jint radius,
                                                              jint type) {
    const jsize length = env->GetArrayLength(oldArray);
    jintArray newArray = env->NewIntArray(length);
    int *oarr = env->GetIntArrayElements(oldArray, NULL);
    int *blurredPixels = env->GetIntArrayElements(newArray, NULL);

    auto start = high_resolution_clock::now();

    Approach current = static_cast<Approach>(type);
    switch (current)
    {
        case Approach::SatBoxCPU:
            satBoxCpu(oarr, blurredPixels, 512, 512, radius);
            break;
        case Approach::BoxCpu:
            boxCpu(oarr, blurredPixels, 512, 512, radius);
            break;
        case Approach::BoxSlidingCPU:
            boxSlidingCpu(oarr, blurredPixels, 512, 512, radius);
            break;
    }

    auto stop = high_resolution_clock::now();
    exec_time = duration_cast<milliseconds>(stop - start);
    dwDuration = exec_time.count();

    env->ReleaseIntArrayElements(oldArray, oarr, NULL);

    return newArray;
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_example_memorybandwidth_MyGLRenderer_getTime(JNIEnv *env, jobject thiz) {
    std::string hello = "Hello from C++";
    return dwDuration;
}

