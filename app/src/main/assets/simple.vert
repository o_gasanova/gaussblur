attribute vec4 aPosition;

varying vec2 vTexCoord;

void main() {
    gl_Position = aPosition * vec4(2.0, 2.0, 2.0, 1.0) - vec4(1.0, 1.0, 0.0, 0.0);
    vTexCoord = aPosition.xy;
}