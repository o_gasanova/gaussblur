precision highp float;

uniform sampler2D tex0;
varying vec2 vTexCoord;
uniform float weights[3];
uniform float iarr[3];
uniform float r[3];


vec4 get_integral(float x0, float y0, float x1, float y1)
{
    float res = 1.0 / 512.0;
    vec4 color = texture2D(tex0, vec2(x1, y1) * res);
    color -= texture2D(tex0,vec2(x0 - 1.0, y1      ) * res);
    color -= texture2D(tex0,vec2(x1      , y0 - 1.0) * res);
    color += texture2D(tex0,vec2(x0 - 1.0, y0 - 1.0) * res);

    return color;
}

void main() {

    vec2 texOffset = vec2(1.0/512.0, 1.0/512.0);
    vec4 color = vec4(0.0,0.0,0.0,0.0);
    uvec2 uv = uvec2(vTexCoord * uvec2(512.0, 512.0));

    for(int ir = 0; ir < 1; ir++)
    {
        float i0 = uv.x - r[ir];
        float i1 = uv.y - r[ir];
        float i2 = uv.x + r[ir];
        float i3 = uv.y + r[ir];

        float i00 = max(i0, 0.0);
        float i11 = max(i1, 0.0);
        float i22 = min(i2, 512.0);
        float i33 = min(i3, 512.0);

        float resInv = 1.0 / 512.0;
        vec4 sumR = get_integral(i00, i11, i22, i33);

        color+= sumR*weights[ir]*iarr[ir];
    }
    gl_FragColor = color;

}