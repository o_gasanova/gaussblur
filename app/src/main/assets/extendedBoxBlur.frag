precision mediump float;

uniform sampler2D tex0;
uniform float numBlurPixelsPerSide;
uniform vec2 coeff;
uniform vec2 texOffset;
varying vec2 vTexCoord;

void main() {
    float coefficientSum = coeff.x;
    vec4 color = texture2D(tex0, vTexCoord)  * coeff.x;
    vec2 uv = vTexCoord;
    for (float i = 1.0; i <= numBlurPixelsPerSide; i++) {
       color += texture2D(tex0, uv + i * texOffset) * coeff.x;
       color += texture2D(tex0, uv - i * texOffset) * coeff.x;
       coefficientSum += coeff.x * 2.0;
    }
    color += texture2D(tex0, uv + (numBlurPixelsPerSide + 1.0) * texOffset) * coeff.y;
    color += texture2D(tex0, uv - (numBlurPixelsPerSide + 1.0) * texOffset) * coeff.y;
    coefficientSum += coeff.y * 2.0;
    gl_FragColor = color/coefficientSum;
}