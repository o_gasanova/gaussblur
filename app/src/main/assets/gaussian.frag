precision mediump float;

uniform sampler2D tex0;
uniform float numBlurPixelsPerSide;
uniform vec4 gIncrementalGaussian;
uniform vec2 texOffset;
varying vec2 vTexCoord;

void main() {
    vec4 avgValue = vec4(0.0);
    float coefficientSum = 0.0;
    vec4 incrementalGaussian = gIncrementalGaussian;

    vec2 uv = vTexCoord;
    vec4 color = texture2D(tex0, uv);
    avgValue += color * incrementalGaussian.x;
    coefficientSum += incrementalGaussian.x;
    incrementalGaussian.xy *= incrementalGaussian.yz;
    for (float i = 1.0; i <= numBlurPixelsPerSide; i++) {
        avgValue += texture2D(tex0, uv - i * texOffset) * incrementalGaussian.x;
        avgValue += texture2D(tex0, uv + i * texOffset) * incrementalGaussian.x;
        coefficientSum += 2.0 * incrementalGaussian.x;
        incrementalGaussian.xy *= incrementalGaussian.yz;
    }
    gl_FragColor = avgValue / coefficientSum;
}