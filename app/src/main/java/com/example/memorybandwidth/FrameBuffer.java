package com.example.memorybandwidth;

import android.opengl.GLES30;

public class FrameBuffer implements AutoCloseable {
    private final int mFramebuffer;
    private boolean mDisposed = false;
    private Texture2D mTexture = null;

    public FrameBuffer() {
        int[] temp = {0};
        GLES30.glGenFramebuffers(1, temp, 0);
        GLE.check();
        mFramebuffer = temp[0];
    }

    public FrameBuffer(Texture2D tex) {
        this();
        attach(tex);
    }

    public int handle() {
        return mFramebuffer;
    }

    public Texture2D texture() {
        return mTexture;
    }

    public void attach(Texture2D texture2D) {
        try (FramebufferBinder framebufferBinder = bind()) {
            GLES30.glFramebufferTexture2D(GLES30.GL_FRAMEBUFFER, GLES30.GL_COLOR_ATTACHMENT0, GLES30.GL_TEXTURE_2D, texture2D.handle(), 0);
            GLE.check();
            mTexture = texture2D;
        }
    }

    public FramebufferBinder bind() {
        return new FramebufferBinder(this);
    }

    public FramebufferBinder bind(Texture2D tex) {
        FramebufferBinder binder = bind();
        GLES30.glFramebufferTexture2D(GLES30.GL_FRAMEBUFFER, GLES30.GL_COLOR_ATTACHMENT0, GLES30.GL_TEXTURE_2D, tex.handle(), 0);
        GLE.check();
        mTexture = tex;
        return binder;
    }

    @Override
    public void close() {
        if (mDisposed)
            return;
        GLES30.glDeleteFramebuffers(1, new int[]{mFramebuffer}, 0);
        mDisposed = true;
    }

    @Override
    protected void finalize() {
        close();
    }

    public static class FramebufferBinder implements AutoCloseable {
        private final int[] mLastState = {0, 0, 0, 0, 0};

        private FramebufferBinder(FrameBuffer frameBuffer) {
            GLES30.glGetIntegerv(GLES30.GL_VIEWPORT, mLastState, 0);
            GLE.check();

            GLES30.glGetIntegerv(GLES30.GL_FRAMEBUFFER_BINDING, mLastState, 4);
            GLE.check();

            GLES30.glBindFramebuffer(GLES30.GL_FRAMEBUFFER, frameBuffer.handle());
            GLE.check();

            Texture2D colorBuf = frameBuffer.texture();
            if (colorBuf != null) {
                GLES30.glViewport(0, 0, colorBuf.width(), colorBuf.height());
                GLE.check();
            }
        }

        @Override
        public void close() {
            GLES30.glViewport(mLastState[0], mLastState[1], mLastState[2], mLastState[3]);
            GLE.check();

            GLES30.glBindFramebuffer(GLES30.GL_FRAMEBUFFER, mLastState[4]);
            GLE.check();
        }
    }
}
