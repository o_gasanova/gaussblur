package com.example.memorybandwidth;

import android.content.Context;
import android.opengl.GLES30;
import android.util.Log;

import java.io.IOException;
import java.io.InputStreamReader;

public class Shader implements AutoCloseable {
    private final int mShaderHandle;
    private boolean mDisposed = false;

    public Shader(Context appCtx, String filename, int type) throws IOException {
        try (InputStreamReader isr = new InputStreamReader(appCtx.getAssets().open(filename))) {
            java.util.Scanner s = new java.util.Scanner(isr).useDelimiter("\\A");
            String source = s.next();

            mShaderHandle = GLES30.glCreateShader(type);
            GLE.check();

            GLES30.glShaderSource(mShaderHandle, source);
            GLE.check();

            GLES30.glCompileShader(mShaderHandle);
            GLE.check();

            int[] compiled = new int[1];
            GLES30.glGetShaderiv(mShaderHandle, GLES30.GL_COMPILE_STATUS, compiled,0);
            String str2;
            if (compiled[0] == 0) {
                str2 = GLES30.glGetShaderInfoLog(mShaderHandle);
            }

            Log.i("OpenGL", GLES30.glGetShaderInfoLog(mShaderHandle));
        }
    }

    int getShaderHandle() throws RuntimeException {
        if (mDisposed)
            throw new RuntimeException("disposed");
        return mShaderHandle;
    }

    @Override
    public void close() {
        if (mDisposed)
            return;
        GLES30.glDeleteShader(mShaderHandle);
        GLE.check();
        mDisposed = true;
    }

    @Override
    protected void finalize() {
        close();
    }
}
