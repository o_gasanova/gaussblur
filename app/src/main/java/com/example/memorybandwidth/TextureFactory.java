package com.example.memorybandwidth;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Random;
import java.lang.Object;


public class TextureFactory {
    @RequiresApi(api = Build.VERSION_CODES.O)
    static Texture2D loadBitmap(String pathname, boolean generateSat)
    {
        File imgFile = new File(pathname);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

        ByteBuffer imageData2 = ByteBuffer.allocateDirect(myBitmap.getWidth() * myBitmap.getHeight() * 4);
        imageData2.order(ByteOrder.nativeOrder());
        myBitmap.copyPixelsToBuffer(imageData2);
        ByteBuffer imageData1 = ByteBuffer.allocateDirect(myBitmap.getWidth() * myBitmap.getHeight() * 4);
        imageData1.order(ByteOrder.nativeOrder());

        int[] newPixels = new int[512 * 512 * 4];

        int i = 0;
        try {
            for (int y = 0; y < 512; ++y)
                for (int x = 0; x < 512; ++x) {
                    byte r = imageData2.get(i);
                    byte g = imageData2.get(i + 1);
                    byte b = imageData2.get(i + 2);
                    byte a = imageData2.get(i + 3);

                    newPixels[i] = Byte.toUnsignedInt(r);
                    newPixels[i+1] = Byte.toUnsignedInt(g);
                    newPixels[i+2] = Byte.toUnsignedInt(b);
                    newPixels[i+3] = 1;
                    i+=4;
                    imageData1.put(r);
                    imageData1.put(g);
                    imageData1.put(b);
                    imageData1.put(a);


                }
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

        imageData1.position(0);
        Texture2D result = new Texture2D();
        result.mBitmap = myBitmap;
        result.kCompress = false;
        if(generateSat){
            result.sat = result.generateSat(imageData2, myBitmap.getWidth(), myBitmap.getHeight());
        }
        else{
            result.sat = newPixels;
            result.upload(myBitmap.getWidth(), myBitmap.getHeight(), imageData1);
        }

        return result;
    }

    static Texture2D createRandom(int width, int height, int seed)
    {
        final int bytes = width * height * 3;
        byte[] data = new byte[bytes];
        Random rand = new Random(seed);
        rand.nextBytes(data);

        ByteBuffer imageData1 = ByteBuffer.allocateDirect(bytes);
        imageData1.order(ByteOrder.nativeOrder());
        imageData1.put(data);

        imageData1.position(0);
        Texture2D result = new Texture2D();
        result.upload(width, height, imageData1);

        return result;
    }
}
