package com.example.memorybandwidth;

import android.graphics.Bitmap;
import android.opengl.ETC1;
import android.opengl.GLES30;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

public class Texture2D implements AutoCloseable {
    boolean kCompress = true;
    private final int mTexture2D;
    private boolean mDisposed = false;
    private int mWidth = 0;
    private int mHeight = 0;
    int[] sat = null;
    Bitmap mBitmap = null;

    public Texture2D() {
        int[] temp = {0};
        GLES30.glGenTextures(1, temp, 0);
        GLE.check();
        mTexture2D = temp[0];
    }

    public Texture2D(int width, int height) {
        this();
        upload(width, height, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public int[] generateSat(ByteBuffer pixels, int mWidth, int mHeight){
        this.mWidth = mWidth;
        this.mHeight = mHeight;
        int numChannels = 4;
        int[] newPixels = new int[mWidth * mHeight * numChannels];
        String res;
        int value = 0;
        try{
            for (int y = 0; y < mHeight; ++y) {
                for (int x = 0; x < mWidth * numChannels; ++x) {
                    byte pixel = pixels.get(y * mWidth * numChannels + x);
                    value = Byte.toUnsignedInt(pixel);
                    if (y > 0)
                        value += newPixels[(y - 1) * mWidth * numChannels + x];
                    if (x >= numChannels)
                        value += newPixels[y * mWidth * numChannels + x - numChannels];
                    if (x >= numChannels && y > 0)
                        value -= newPixels[(y - 1) * mWidth * numChannels + x - numChannels];
                    newPixels[y * mWidth * numChannels + x ] = value;
                }
            }
        }
        catch (RuntimeException e) {
            res = e.getMessage();
        }

        return newPixels;
    }

    public void upload(int width, int height, Buffer data) {
        try (Texture2DBinder binder = bind(31)) {

            GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_LINEAR);
            GLE.check();

            GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_LINEAR);
            GLE.check();

            if (!kCompress || data == null) {
                GLES30.glTexImage2D(GLES30.GL_TEXTURE_2D, 0, GLES30.GL_RGBA, width, height, 0, GLES30.GL_RGBA, GLES30.GL_UNSIGNED_BYTE, data);
                GLE.check();
            } else {
                final int imageBytesSize = ETC1.getEncodedDataSize(width, height);
                ByteBuffer compressedBuffer = ByteBuffer.allocateDirect(imageBytesSize).order(ByteOrder.nativeOrder());
                ETC1.encodeImage(data, width, height, 3, 3 * width, compressedBuffer);

                GLES30.glCompressedTexImage2D(GLES30.GL_TEXTURE_2D, 0, ETC1.ETC1_RGB8_OES, width, height, 0, imageBytesSize, compressedBuffer);
                GLE.check();
            }

            mWidth = width;
            mHeight = height;
        }
    }

    public void uploadArray(int width, int height, int[] pixels) {
        try (Texture2DBinder binder = bind(31)) {

            GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_LINEAR);
            GLE.check();

            GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_LINEAR);
            GLE.check();

            ByteBuffer data = ByteBuffer.allocateDirect(pixels.length * 4);
            data.order(ByteOrder.nativeOrder());
            IntBuffer b = data.asIntBuffer();
            for (int i = 0; i < pixels.length; ++i) {
                data.put((byte)pixels[i]);
            }
            data.position(0);

            GLES30.glTexImage2D(GLES30.GL_TEXTURE_2D, 0, GLES30.GL_RGBA, width, height, 0,
                    GLES30.GL_RGBA, GLES30.GL_UNSIGNED_BYTE, data);
            //GLES30.glTexImage2D(GLES30.GL_TEXTURE_2D, 0, GLES30.GL_RGBA32I, width, height, 0,
            //        GLES30.GL_RGBA_INTEGER, GLES30.GL_INT, data);
            GLE.check();

            mWidth = width;
            mHeight = height;
        }
    }

    public int handle() {
        return mTexture2D;
    }

    public int width() {
        return mWidth;
    }

    public int height() {
        return mHeight;
    }

    public Texture2DBinder bind(int slot) {
        return new Texture2DBinder(this, slot);
    }

    @Override
    public void close() {
        if (mDisposed)
            return;
        GLES30.glDeleteTextures(1, new int[]{mTexture2D}, 0);
        mDisposed = true;
    }

    @Override
    protected void finalize() {
        close();
    }

    public static class Texture2DBinder implements AutoCloseable {
        private final int mLastSlot;
        private final int mLastTexture;


        private Texture2DBinder(Texture2D texture2D, int slot) {
            int[] temp = {0};
            GLES30.glGetIntegerv(GLES30.GL_ACTIVE_TEXTURE, temp, 0);
            GLE.check();
            mLastSlot = temp[0];

            GLES30.glActiveTexture(GLES30.GL_TEXTURE0 + slot);
            GLE.check();

            GLES30.glGetIntegerv(GLES30.GL_TEXTURE_BINDING_2D, temp, 0);
            GLE.check();
            mLastTexture = temp[0];

            GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, texture2D.handle());
            GLE.check();
        }

        @Override
        public void close() {
            GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, mLastTexture);
            GLE.check();

            GLES30.glActiveTexture(mLastSlot);
            GLE.check();
        }
    }
}
