package com.example.memorybandwidth;

import android.opengl.GLES30;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Square {
    static float[] squareCoords = {
            0.0f, 0.0f,
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
    };
    static short[] indices = {0, 1, 2, 0, 2, 3};

    private final FloatBuffer vertexBuffer;
    private final ShortBuffer indicesBuffer;

    public Square() {
        ByteBuffer bb = ByteBuffer.allocateDirect(squareCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(squareCoords);
        vertexBuffer.position(0);

        ByteBuffer dlb = ByteBuffer.allocateDirect(indices.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        indicesBuffer = dlb.asShortBuffer();
        indicesBuffer.put(indices);
        indicesBuffer.position(0);

    }

    public void draw(ShaderProgram program, Texture2D mainTex) throws Exception {

        try (Texture2D.Texture2DBinder ignored = mainTex.bind(0)) {
            try (ShaderProgram.ShaderActivator sa = program.activate()) {

                int positionHandle = program.getAttribute("aPosition");
                int tex0Handler = program.getUniform("tex0");

                GLES30.glEnableVertexAttribArray(positionHandle);
                GLE.check();

                GLES30.glVertexAttribPointer(positionHandle, 3, GLES30.GL_FLOAT, false, 3 * 4, vertexBuffer);
                GLE.check();

                GLES30.glUniform1i(tex0Handler, 0);
                GLE.check();

                GLES30.glDrawElements(GLES30.GL_TRIANGLES, 3 * 2, GLES30.GL_UNSIGNED_SHORT, indicesBuffer);
                GLE.check();

                GLES30.glDisableVertexAttribArray(positionHandle);
                GLE.check();
            }
        }
    }

    public void drawGaussianBlur(ShaderProgram program, Texture2D mainTex, float incrementalGaussian[], float numPixels, float[] texOffset) throws Exception {

        try (Texture2D.Texture2DBinder ignored = mainTex.bind(0)) {
            try (ShaderProgram.ShaderActivator sa = program.activate()) {

                int tex0Handler = program.getUniform("tex0");
                int numBlurPixelsHandler = program.getUniform("numBlurPixelsPerSide");
                int incrementalGaussianHandler = program.getUniform("gIncrementalGaussian");
                int texOffsetHandler = program.getUniform("texOffset");

                int positionHandle = program.getAttribute("aPosition");
                GLES30.glEnableVertexAttribArray(positionHandle);
                GLE.check();

                GLES30.glVertexAttribPointer(positionHandle, 3, GLES30.GL_FLOAT, false, 3 * 4, vertexBuffer);
                GLE.check();

                GLES30.glUniform1i(tex0Handler, 0);
                GLE.check();
                GLES30.glUniform1f(numBlurPixelsHandler, numPixels);
                GLE.check();
                GLES30.glUniform4f(incrementalGaussianHandler, incrementalGaussian[0], incrementalGaussian[1], incrementalGaussian[2], incrementalGaussian[3]);
                GLE.check();
                GLES30.glUniform2f(texOffsetHandler, texOffset[0], texOffset[1]);
                GLE.check();

                GLES30.glDrawElements(GLES30.GL_TRIANGLES, 3 * 2, GLES30.GL_UNSIGNED_SHORT, indicesBuffer);
                GLE.check();

                GLES30.glDisableVertexAttribArray(positionHandle);
                GLE.check();
            }
        }
    }

    public void drawExtendedBlur(ShaderProgram program, Texture2D mainTex, float coeffs[], float numPixels, float[] texOffset) throws Exception {

        try (Texture2D.Texture2DBinder ignored = mainTex.bind(0)) {
            try (ShaderProgram.ShaderActivator sa = program.activate()) {

                int tex0Handler = program.getUniform("tex0");
                int numBlurPixelsHandler = program.getUniform("numBlurPixelsPerSide");
                int coeffHandler = program.getUniform("coeff");
                int texOffsetHandler = program.getUniform("texOffset");

                int positionHandle = program.getAttribute("aPosition");
                GLES30.glEnableVertexAttribArray(positionHandle);
                GLE.check();

                GLES30.glVertexAttribPointer(positionHandle, 3, GLES30.GL_FLOAT, false, 3 * 4, vertexBuffer);
                GLE.check();

                GLES30.glUniform1i(tex0Handler, 0);
                GLE.check();
                GLES30.glUniform1f(numBlurPixelsHandler, numPixels);
                GLE.check();
                GLES30.glUniform2f(coeffHandler, coeffs[0], coeffs[1]);
                GLE.check();
                GLES30.glUniform2f(texOffsetHandler, texOffset[0], texOffset[1]);
                GLE.check();

                GLES30.glDrawElements(GLES30.GL_TRIANGLES, 3 * 2, GLES30.GL_UNSIGNED_SHORT, indicesBuffer);
                GLE.check();

                GLES30.glDisableVertexAttribArray(positionHandle);
                GLE.check();
            }
        }
    }

    public void drawSatBlur(ShaderProgram program, Texture2D mainTex, float iarr[],float weights[],float r[]) throws Exception {
        try (Texture2D.Texture2DBinder ignored = mainTex.bind(0)) {
            try (ShaderProgram.ShaderActivator sa = program.activate()) {

                int tex0Handler = program.getUniform("tex0");
                int iarrHandler = program.getUniform("iarr[0]");
                int weightsHandler = program.getUniform("weights[0]");
                int rHandler = program.getUniform("r[0]");

                int positionHandle = program.getAttribute("aPosition");
                GLES30.glEnableVertexAttribArray(positionHandle);
                GLE.check();

                GLES30.glVertexAttribPointer(positionHandle, 3, GLES30.GL_FLOAT, false, 3 * 4, vertexBuffer);
                GLE.check();

                GLES30.glUniform1i(tex0Handler, 0);
                GLE.check();

                ByteBuffer byteBuf = ByteBuffer.allocateDirect(iarr.length * Float.BYTES); //4 bytes per float
                byteBuf.order(ByteOrder.nativeOrder());
                FloatBuffer buffer = byteBuf.asFloatBuffer();
                buffer.put(iarr);
                buffer.position(0);
                GLES30.glUniform1fv(iarrHandler, 3, buffer);

                ByteBuffer byteBuf2 = ByteBuffer.allocateDirect(weights.length * Float.BYTES); //4 bytes per float
                byteBuf2.order(ByteOrder.nativeOrder());
                FloatBuffer buffer2 = byteBuf2.asFloatBuffer();
                buffer2.put(weights);
                buffer2.position(0);
                GLES30.glUniform1fv(weightsHandler, 3, buffer2);

                ByteBuffer byteBuf3 = ByteBuffer.allocateDirect(r.length * Float.BYTES); //4 bytes per float
                byteBuf3.order(ByteOrder.nativeOrder());
                FloatBuffer buffer3 = byteBuf3.asFloatBuffer();
                buffer3.put(r);
                buffer3.position(0);
                GLES30.glUniform1fv(rHandler, 3, buffer3);

                GLES30.glDrawElements(GLES30.GL_TRIANGLES, 3 * 2, GLES30.GL_UNSIGNED_SHORT, indicesBuffer);
                GLE.check();

                GLES30.glDisableVertexAttribArray(positionHandle);
                GLE.check();
            }
        }
    }
}