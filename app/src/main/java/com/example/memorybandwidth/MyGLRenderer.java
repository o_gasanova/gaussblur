package com.example.memorybandwidth;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static org.opencv.core.Core.setNumThreads;

public class MyGLRenderer implements GLSurfaceView.Renderer {
    private static final String TAG = "GAUSS TIME";
    static {
        System.loadLibrary("GaussBlur");
    }

    final Context mContext;
    ShaderProgram mBlitProgram = null;
    ShaderProgram mSatBoxProgram = null;
    ShaderProgram mFilterProgram = null;
    ShaderProgram mExtendedBlurProgram = null;
    ShaderProgram mIncrementalGaussianProgram = null;

    Texture2D mDisplace = null;
    Texture2D mLennaBitmap = null;
    Texture2D mLennaSatBitmap = null;
    Texture2D mColorBuffer0 = null;
    Texture2D mColorBuffer1 = null;
    FrameBuffer mFrameBuffer = null;
    Square mSquare = null;
    float numBlurPixelsPerSide;
    float[] coeffs = new float[2];
    float[] horTexOffset = new float[2];
    float[] verTexOffset = new float[2];
    float[] incrementalGaussian = new float[4];
    int[] blurredPixels = new int[512 * 512 * 4];
    int[] blurredPixels2 = new int[512 * 512 * 4];
    int[] pixelsCopy = new int[512 * 512 * 4];
    int[] decodedSat = new int[512 * 512 * 4];
    float mRadius = 5.0f;

    int numChannels = 4;

    MyGLRenderer(Context ctx) {
        mContext = ctx;
    }
    public native int[] BoxCpuCommonCpp(int[] data, int radius, int type);

    public native int getTime();

    public void initParams() {

        float sigma2, L, l, a;
        int passes = 3;
        sigma2 = mRadius * mRadius / passes;
        L = (float) Math.sqrt(12.0f * sigma2 + 1.0f);
        l = (float) Math.floor((L - 1.0) / 2.0);
        a = (2 * l + 1) * (l * (l + 1) - 3 * sigma2);
        a /= 6 * (sigma2 - (l + 1) * (l + 1));
        float floatRadius = l + a;

        int iradius = (int) floatRadius;
        float ww = a / (floatRadius * 2 + 1);
        float fw = (1 - a) / (floatRadius * 2 + 1);

        numBlurPixelsPerSide = (float) iradius;
        coeffs[0] = ww;
        coeffs[1] = fw;
        horTexOffset[0] = 1.0f / 512.f;
        horTexOffset[1] = 0.f;
        verTexOffset[0] = 0.f;
        verTexOffset[1] = 1.0f / 512.f;

        float sigma = mRadius*2.0f / ((float) Math.log(255) - 1.0f);
        incrementalGaussian[0] = 1.0f / ((float)Math.sqrt(2.0f * Math.PI) * sigma);
        incrementalGaussian[1] = (float)Math.exp(-0.5f / (sigma * sigma));
        incrementalGaussian[2] = incrementalGaussian[1] * incrementalGaussian[1];
        incrementalGaussian[3] = 0.0f;

        if (!OpenCVLoader.initDebug()) {
            Log.d("ERROR","Failed to load Opencv");
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLE.check();

        try {
            Shader simpleVertex = new Shader(mContext, "simple.vert", GLES30.GL_VERTEX_SHADER);
            Shader simpleFrag = new Shader(mContext, "simple.frag", GLES30.GL_FRAGMENT_SHADER);
            Shader extendedBlurFrag = new Shader(mContext, "extendedBoxBlur.frag", GLES30.GL_FRAGMENT_SHADER);
            Shader gaussianBlurFrag = new Shader(mContext, "gaussian.frag", GLES30.GL_FRAGMENT_SHADER);
            Shader satBoxFrag = new Shader(mContext, "satBox.frag", GLES30.GL_FRAGMENT_SHADER);

            mBlitProgram = new ShaderProgram(simpleVertex, simpleFrag);
            mExtendedBlurProgram = new ShaderProgram(simpleVertex, extendedBlurFrag);
            mIncrementalGaussianProgram = new ShaderProgram(simpleVertex, gaussianBlurFrag);
            mSatBoxProgram = new ShaderProgram(simpleVertex, satBoxFrag);

            final int kWidth = 512;
            final int kHeight = 512;

            mLennaBitmap = TextureFactory.loadBitmap("/sdcard/Pictures/Lenna512.png", false);
            mLennaSatBitmap = TextureFactory.loadBitmap("/sdcard/Pictures/Lenna512.png", true);
            mLennaSatBitmap.uploadArray(512, 512, mLennaSatBitmap.sat);

            mColorBuffer0 = new Texture2D(kWidth, kHeight);
            mColorBuffer1 = new Texture2D(kWidth, kHeight);

            mFrameBuffer = new FrameBuffer();
            mSquare = new Square();

            initParams();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void SatBoxCpuBlur(int[] source, int[] output, int width, int height, int radius) {
        ArrayList<Integer> gaussianBoxes = CreateGausianBoxes(radius, 3);
        satBlur(source, output, width, height, (gaussianBoxes.get(0) - 1) / 2);
        satBlur(output, source, width, height, (gaussianBoxes.get(1) - 1) / 2);
        satBlur(source, output, width, height, (gaussianBoxes.get(2) - 1) / 2);
    }

    private void FastGaussianBlurExt(int[] source, int[] output, int width, int height, int radius) {
        ArrayList<Integer> gaussianBoxes = CreateGausianBoxes(radius, 3);
        BoxSlidingF(source, output, width, height, (gaussianBoxes.get(0) - 1) / 2);
        BoxSlidingF(source, output, width, height, (gaussianBoxes.get(1) - 1) / 2);
        BoxSlidingF(source, output, width, height, (gaussianBoxes.get(2) - 1) / 2);
    }

    private ArrayList<Integer> CreateGausianBoxes(double sigma, int n) {
        double idealFilterWidth = Math.sqrt((12 * sigma * sigma / n) + 1);

        int filterWidth = (int) Math.floor(idealFilterWidth);
        if (filterWidth % 2 == 0) {
            filterWidth--;
        }

        int filterWidthU = filterWidth + 2;

        double mIdeal = (12 * sigma * sigma - n * filterWidth * filterWidth - 4 * n * filterWidth - 3 * n) / (-4 * filterWidth - 4);
        double m = Math.round(mIdeal);

        ArrayList<Integer> result = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            result.add(i < m ? filterWidth : filterWidthU);
        }

        return result;
    }

    private void BoxSlidingF(int[] source, int[] output, int width, int height, int radius) {
        boxBlurH_3(source, output, width * 4, height, radius);
        boxSlidingV(output, source, width * 4, height, radius);
    }

    private int get_integral(int[] scl, int x0, int y0, int x1, int y1, int w, int h)
    {
        int sum = scl[y1 * h * numChannels + x1];

        if (x0 >= numChannels)
            sum -= scl[y1* h * numChannels + x0 - numChannels];
        if (y0 > 0)
            sum -= scl[(y0 - 1) * h * numChannels + x1];
        if (x0 >= numChannels && y0 > 0)
            sum += scl[(y0 - 1) * h * numChannels + x0 - numChannels];

        return sum;
    }

    private void satBlur (int[] scl, int[] tcl, int w, int h, int r) {
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w * numChannels; x++) {

                int i00 = Math.max(x - r * numChannels, 0);
                int i11 = Math.max(y - r, 0);
                int i22 = Math.min(x + r * numChannels, w * numChannels - 1);
                int i33 = Math.min(y + r, h - 1);

                int sumR = (int) get_integral(scl,
                        i00, i11, i22, i33,
                        w, h);

                float ks = r + r + 1;
                float iarr = 1f / (ks * ks);

                int res = Math.round(sumR * iarr);
                if(x >= numChannels && y> 0)
                    res -= tcl[x - numChannels + (y - 1) * h * numChannels];
                if(y > 0)
                    res += tcl[x + (y - 1) * h * numChannels];
                if(x >= numChannels)
                    res += tcl[x - numChannels + y * h * numChannels];

                tcl[y * h * numChannels + x] = res;
            }
        }
    }

    private void boxBlurV_3 (int[] scl, int[] tcl, int w, int h, int r) {
        for(int i=0; i<h; i++)
            for(int j=0; j<w; j++) {
                int val = 0;
                for(int iy=i-r; iy<i+r+1; iy++) {
                    int y = Math.min(h-1, Math.max(0, iy));
                    val += scl[y*w+j];
                }
                tcl[i*w+j] = val/(r+r+1);
            }
    }

    private void boxBlurH_3 (int[] scl, int[] tcl, int w, int h, int r) {
        for(int i=0; i<h; i++)
            for(int j=0; j<w; j++)  {
                int val = 0;
                for(int ix=j-r*4; ix<j+(r+1)*4; ix=ix+4) {
                    int x = Math.min(w-1, Math.max(0, ix));
                    val += scl[i*w+x];
                }
                tcl[i*w+j] = val/(r+r+1);
            }
    }

    void boxSlidingH (int[] scl, int[] tcl, int w, int h, int r) {
        float iarr = 1.0f / (r+r+1);
        for(int i = 0; i<h; i++) {
            int ti = i * w, li = ti, ri = ti + r * 4;
            int fv = scl[ti], lv = scl[ti + w - 1], val = (r + 1) * fv;

            int acc0 = (r + 1) * scl[i * w];
            int acc1 = (r + 1) * scl[i * w + 1];
            int acc2 = (r + 1) * scl[i * w + 2];
            int acc3 = (r + 1) * scl[i * w + 3];

            for (int j = 0; j < r * numChannels; j += 4) {
                acc0 += scl[ti + j];
                acc1 += scl[ti + j + 1];
                acc2 += scl[ti + j + 2];
                acc3 += scl[ti + j + 3];
            }
            for (int j = 0; j <= r * 4; j += 4) {
                acc0 += scl[ri++] - scl[i * w];
                acc1 += scl[ri++] - scl[i * w + 1];
                acc2 += scl[ri++] - scl[i * w + 2];
                acc3 += scl[ri++] - scl[i * w + 3];

                tcl[ti++] = Math.round(acc0 * iarr);
                tcl[ti++] = Math.round(acc1 * iarr);
                tcl[ti++] = Math.round(acc2 * iarr);
                tcl[ti++] = Math.round(acc3 * iarr);
            }
            for (int j = r * 4 + 4; j < w - 4; j += 4) {
                acc0 += scl[ri++] - scl[li++];
                acc1 += scl[ri++] - scl[li++];
                acc2 += scl[ri++] - scl[li++];
                acc3 += scl[ri++] - scl[li++];

                tcl[ti++] = Math.round(acc0 * iarr);
                tcl[ti++] = Math.round(acc1 * iarr);
                tcl[ti++] = Math.round(acc2 * iarr);
                tcl[ti++] = Math.round(acc3 * iarr);
            }
            for (int j = w - r * 4; j < w; j+=4) {
                acc0 += scl[ti + w] - scl[li++];
                acc1 += scl[ti + w - 1] - scl[li++];
                acc2 += scl[ti + w - 2] - scl[li++];
                acc3 += scl[ti + w - 3] - scl[li++];

                tcl[ti++] = Math.round(acc0 * iarr);
                tcl[ti++] = Math.round(acc1 * iarr);
                tcl[ti++] = Math.round(acc2 * iarr);
                tcl[ti++] = Math.round(acc3 * iarr);
            }
        }
    }

    private void boxSlidingV (int[] scl, int[] tcl, int w, int h, int r) {
        float iarr = 1f / (r+r+1f);
        for(int i=0; i<w; i++) {
            int ti = i, li = ti, ri = ti+r*w;
            int fv = scl[ti], lv = scl[ti+w*(h-1)], val = (r+1)*fv;
            for(int j=0; j<r; j++) val += scl[ti+j*w];
            for(int j=0  ; j<=r ; j++) { val += scl[ri] - fv     ;  tcl[ti] = Math.round(val*iarr);  ri+=w; ti+=w; }
            for(int j=r+1; j<h-r; j++) { val += scl[ri] - scl[li];  tcl[ti] = Math.round(val*iarr);  li+=w; ri+=w; ti+=w; }
            for(int j=h-r; j<h  ; j++) { val += lv      - scl[li];  tcl[ti] = Math.round(val*iarr);  li+=w; ti+=w; }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void BoxCpuCommon(int type){
        try {

            int[] pixels = type == Approach.SatBoxCPU.ordinal() ? mLennaSatBitmap.sat : mLennaBitmap.sat;
            int numChannels = 4;
            int width = mLennaSatBitmap.width();
            int height = mLennaSatBitmap.height();

            System.arraycopy(pixels, 0, pixelsCopy, 0, pixelsCopy.length);

            blurredPixels = BoxCpuCommonCpp(pixelsCopy, (int)mRadius, type);
            long time = getTime();
            Log.d(TAG,"Total execution time: " + time + "ms");

            /*for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width * numChannels; x++)
                {
                    int A = 0, B = 0, C = 0, D = 0;
                    if(x >= numChannels && y> 0)
                        A = blurredPixels[x - numChannels + (y - 1) * width * numChannels];
                    if(y > 0)
                        B = blurredPixels[x + (y - 1) * width * numChannels];
                    if(x >= numChannels)
                        C = blurredPixels[x - numChannels + y * width * numChannels];

                    D = blurredPixels[x + y * width * numChannels];

                    int sum =  A - B - C + D;
                    decodedSat[y * width * numChannels + x] = sum;
                }
            }*/

            mLennaSatBitmap.uploadArray(width, height, blurredPixels);

            try (FrameBuffer.FramebufferBinder fbb = mFrameBuffer.bind(mColorBuffer1)) {
                mSquare.draw(mBlitProgram, mLennaSatBitmap);

                Texture2D temp = mColorBuffer0;
                mColorBuffer0 = mColorBuffer1;
                mColorBuffer1 = temp;
            }
            mSquare.draw(mBlitProgram, mColorBuffer1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void SatBoxGpu(){
        try {

            int[] pixels = mLennaSatBitmap.sat;
            int numChannels = 4;
            int width = mLennaSatBitmap.width();
            int height = mLennaSatBitmap.height();



            /*for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width * numChannels; x++)
                {
                    int A = 0, B = 0, C = 0, D = 0;
                    if(x >= numChannels && y> 0)
                        A = blurredPixels[x - numChannels + (y - 1) * width * numChannels];
                    if(y > 0)
                        B = blurredPixels[x + (y - 1) * width * numChannels];
                    if(x >= numChannels)
                        C = blurredPixels[x - numChannels + y * width * numChannels];

                    D = blurredPixels[x + y * width * numChannels];

                    int sum =  A - B - C + D;
                    decodedSat[y * width * numChannels + x] = sum;
                }
            }*/
            float r[] = {1.f, 7.f, 15.f};
            float weight[] = { 0.5f,0.3f,0.2f };
            float iarr[] = {0.f,0.f,0.f};
            for (int i = 0; i < 3; i ++)
            {
                float ks = r[i] + r[i] + 1;
                iarr[i] = 1.f / (ks * ks);
            }

            try (FrameBuffer.FramebufferBinder fbb = mFrameBuffer.bind(mColorBuffer1)) {
                mSquare.drawSatBlur(mSatBoxProgram, mLennaSatBitmap, iarr, weight, r);

                Texture2D temp = mColorBuffer0;
                mColorBuffer0 = mColorBuffer1;
                mColorBuffer1 = temp;
            }
            mSquare.draw(mBlitProgram, mColorBuffer1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void ExtendedBoxGpu(){
        try {
            long startTime = System.currentTimeMillis();
            for (int i = 0; i < 6; ++i) {
                try (FrameBuffer.FramebufferBinder fbb = mFrameBuffer.bind(mColorBuffer1)) {

                    mSquare.drawExtendedBlur(mExtendedBlurProgram, i == 0 ? mLennaBitmap : mColorBuffer0, coeffs,
                            numBlurPixelsPerSide, i < 3 ? horTexOffset : verTexOffset);

                    Texture2D temp = mColorBuffer0;
                    mColorBuffer0 = mColorBuffer1;
                    mColorBuffer1 = temp;
                }
            }
            long endTime = System.currentTimeMillis();
            Log.d("HEY","Total execution time: " + (endTime-startTime) + "ms");
            mSquare.draw(mBlitProgram, mColorBuffer1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void IncrementalGaussianGpu(){
        try {
            for (int i = 0; i < 3; i++) {
                try (FrameBuffer.FramebufferBinder fbb = mFrameBuffer.bind(mColorBuffer1)) {
                    mSquare.drawGaussianBlur(mIncrementalGaussianProgram, i == 0 ? mLennaBitmap : mColorBuffer0, incrementalGaussian,
                            numBlurPixelsPerSide, i == 0  ? verTexOffset : horTexOffset);

                    Texture2D temp = mColorBuffer0;
                    mColorBuffer0 = mColorBuffer1;
                    mColorBuffer1 = temp;
                }
            }

            mSquare.draw(mBlitProgram, mColorBuffer1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void OpenCvCpu() {
        try {
            setNumThreads(0);
            Bitmap bitmap = mLennaBitmap.mBitmap.copy(Bitmap.Config.ARGB_8888,true);
            Mat src = new Mat(bitmap.getHeight(),bitmap.getWidth(), CvType.CV_8UC4);
            Mat dst = new Mat(bitmap.getHeight(),bitmap.getWidth(), CvType.CV_8UC4);
            Utils.bitmapToMat(bitmap, src);

            long startTime = System.currentTimeMillis();
            Imgproc.GaussianBlur(src, dst, new Size(11, 11), 0);
            long endTime = System.currentTimeMillis();
            Log.d(TAG,"Total execution time: " + (endTime-startTime) + "ms");

            Bitmap processImg = Bitmap.createBitmap(dst.cols(),dst.rows(),Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(dst, processImg);

            ByteBuffer imageData2 = ByteBuffer.allocateDirect(bitmap.getWidth() * bitmap.getHeight() * numChannels);
            imageData2.order(ByteOrder.nativeOrder());
            processImg.copyPixelsToBuffer(imageData2);
            Texture2D result = new Texture2D();
            result.kCompress = false;
            imageData2.position(0);
            result.upload(bitmap.getWidth(), bitmap.getHeight(), imageData2);

            try (FrameBuffer.FramebufferBinder fbb = mFrameBuffer.bind(mColorBuffer1)) {
                mSquare.draw(mBlitProgram, result);

                Texture2D temp = mColorBuffer0;
                mColorBuffer0 = mColorBuffer1;
                mColorBuffer1 = temp;
            }
            mSquare.draw(mBlitProgram, mColorBuffer1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    enum Approach
    {
        BoxCpu,
        BoxSlidingCPU,
        SatBoxCPU,
        OpenCVCPU,
        ExtendedBoxGPU,
        IncrementalGaussianGPU,
        SatBoxGPU,
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onDrawFrame(GL10 unused) {

        try {
            //extendedBoxBlurGPU();
            long startTime = System.currentTimeMillis();

            Approach current = Approach.SatBoxGPU;
            switch (current) {
                case BoxCpu:
                case BoxSlidingCPU:
                case SatBoxCPU:
                    BoxCpuCommon(current.ordinal());
                    break;
                case OpenCVCPU:
                    OpenCvCpu();
                case ExtendedBoxGPU:
                    for (int i = 0; i < 15; i++)
                    {
                        ExtendedBoxGpu();
                    }
                    break;
                case IncrementalGaussianGPU:
                    for (int i = 0; i < 15; i++)
                    {
                        IncrementalGaussianGpu();
                    }
                    break;
                case SatBoxGPU:
                    for (int i = 0; i < 15; i++)
                    {
                        SatBoxGpu();
                    }
                    break;
                default:
                    //BoxCpuCommon(0);
                    break;
            }


            //openCvBlur();
            //gaussianBlur();
            //extendedBoxBlurCpu();
            long endTime = System.currentTimeMillis();
            //Log.d("HEY","Total execution time: " + (endTime-startTime) + "ms");
            //satCpuBlur();
            //extendedBoxBlurCpu();
            //openCvBlur();



        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        GLE.check();
    }
}