package com.example.memorybandwidth;

import android.opengl.GLSurfaceView;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         final String[] REQUIRED_PERMISSIONS = {"android.permission.READ_EXTERNAL_STORAGE"};
         final int REQUEST_CODE_PERMISSIONS = 10; //arbitrary number, can be changed accordingly

        ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);

        GLSurfaceView gLView = new MyGLSurfaceView(this);
        setContentView(gLView);
    }
}